// const Apify = require('apify');

// Apify.main(async () => {
//     const asins = []
//     const fullResults = []
//     const input = await Apify.getValue('INPUT')

//     const requestQueue = await Apify.openRequestQueue();
//     await requestQueue.addRequest({ url: `https://www.amazon.com/s/?field-keywords=${input.keyword}` });

//     const crawler = new Apify.CheerioCrawler({
//         requestQueue,
//         handlePageFunction: async ({ $, request }) => {
//             $('.s-main-slot.s-result-list.s-search-results.sg-row div').each((i, el) => {
//                 const text = $(el).attr('data-asin')
//                 if (text != undefined && text != '') {
//                     asins.push(text)
//                 }
//             });

//             const detailsProductPages = asins.map(asin => ({
//                 url: `https://www.amazon.com/dp/${asin}`,
//                 userData: {
//                     label: 'PRODUCT',
//                 },
//             }));
        
//             const requestList = await Apify.openRequestList('asins', detailsProductPages);
        
//             const detailCrawler = new Apify.CheerioCrawler({
//                 requestList,
//                 handlePageFunction: async ({$, request}) => {
//                     console.log(`Processing ${request.url}`);
        
//                     const results = {
//                         title: $('#titleSection #productTitle').text().trim(),
//                         url: request.url,
//                         description: $('.a-unordered-list.a-vertical.a-spacing-mini li .a-list-item').text().trim().replace(/\n/g,''),
//                         keyword: input.keyword,
//                         sellerName: $('#tabular-buybox-truncate-1 .tabular-buybox-text').text().trim(),
//                         price: $('.a-box-inner #price_inside_buybox').text().trim() || $('.a-lineitem #priceblock_ourprice'),
//                         shippingPrice: $('.a-box-inner .a-size-base.a-color-secondary').text().trim().replace(/\n/g,'') || $('.a-size-base.a-color-secondary').text().trim().replace(/\n/g,'')
//                     } 
//                     await fullResults.push(results)
//                     await Apify.pushData(fullResults)
//                 }
//             }) 
//             await detailCrawler.run();
//         },
//     });

//     await crawler.run();

// });


// // // const Apify = require('apify')
// // // const {utils: {log}} = Apify

// // // Apify.main(async () => {
// // //     const input = await Apify.getValue('INPUT')
// // //     log.info('Starting actor')
// // //     const firstUrl = `https://www.amazon.com/s/?field-keywords=${input.keyword}`
// // //     const requestList = await Apify.openRequestList('first-url', [{
// // //         url: firstUrl,
// // //         userData: {label: 'SEARCH'}
// // //     }])


// // // const requestQueue = await Apify.openRequestQueue()

// // // const crawler = new Apify.PuppeteerCrawler({
// // //     requestList,
// // //         requestQueue,
// // //         useSessionPool: true,
// // //         persistCookiesPerSession: true,
// // //         sessionPoolOptions: {
// // //             maxPoolSize: 100,
// // //             sessionOptions: {
// // //                 maxUsageCount: 5,
// // //             },
// // //         },
// // //         maxRequestsPerCrawl: 50,
// // //         launchContext: {
// // //             // Chrome with stealth should work for most websites.
// // //             useChrome: false,
// // //             stealth: true,
            
// // //         },


// // //     handlePageFunction: async (context) => {
// // //         const {url, userData: {label}, } = context.request
// // //         const request = context.request
// // //         const page = context.page
        
// // //         switch(request.userData.label) {
// // //             case 'SEARCH':
// // //                 const asins = await page.$$eval('.s-asin', (els) => {
// // //                     return els.map((el) => el.getAttribute('data-asin'));
// // //                 });
            
// // //                 for (const ASIN of asins) {
// // //                     await requestQueue.addRequest({
// // //                         url: `https://www.amazon.com/dp/${ASIN}`,
// // //                         userData: { label: 'PRODUCT', ASIN, keyword },
// // //                     });
// // //                 }
            
// // //             case 'PRODUCT':
// // //                 const pageTitle = page.$eval('#title', (el) => el.textContent.trim())
// // //                 const pageDesription = page.$$eval('#feature-bullets li:not([data-doesntfitmessage])',
// // //                 ((items) => items.map((el) => el.textContent?.trim())))
        
// // //             case 'OFFER':
// // //                 let userData = request.userData

// // //                 let offers = await page.$$eval('.aod-offer-heading', elems => {
// // //                     return elems.map((elem) => {
// // //                         const seller = elem.querySelector('.a-size-small.a-color-base')
// // //                         const price = elem.querySelector('.a-price .a-offscreen')
// // //                         const shippingPrice = elem.querySelector('a-color-secondary.a-size-base')
// // //                         if (!shippingPrice) shippingPrice = 'Not available'
// // //                         return {
// // //                             seller,
// // //                             price,
// // //                             shippingPrice
// // //                         }
// // //                     })
// // //                 })

// // //                 for(let el in offers) {
// // //                     let result = {
// // //                         title: userData.title,
// // //                         url: userData.url,
// // //                         description: userData.description,
// // //                         keyword,
// // //                     }
// // //                     Object.assign(result, el);
// // //                     log.info("Pushing result: ", { result })
// // //                     await Apify.pushData(result);

// // //                 }
// // //             }   
// // //         }
    
// // //     })

// // //     log.info('Starting the crawl.');
// // //     await crawler.run();
// // //     log.info('Crawl finished.');
// // // })


// // // const Apify = require('apify');

// // // Apify.main(async () => {
// // //     const asins = []
// // //     const input = await Apify.getValue('INPUT')

// // //     const requestQueue = await Apify.openRequestQueue();
// // //     await requestQueue.addRequest({ url: `https://www.amazon.com/s/?field-keywords=${input.keyword}`, userData: {label: 'SEARCH'} });

// // //     const crawler = new Apify.PuppeteerCrawler({
// // //         requestQueue,
// // //         requestQueue,
// // //         handlePageFunction: async ({ $, request, page }) => {
// // //             switch(label) {
// // //                 case 'SEARCH'
// // //             }
// // //             asins = await page.$$eval('.s-asin', (products) => {
// // //                 return products.map((el) => el.getAttribute('data-asin'));
// // //             }); 
            
// //             for (const ASIN of asins) {
// //                 await requestQueue.addRequest({
// //                     url: `https://www.amazon.com/dp/${ASIN}`,
// //                     userData: { label: 'PRODUCT', ASIN, keyword },
// //                 });
// //             }
        
// // //         },
// // //     });

// // //     await crawler.run();

// // //     const detailsProductPages = asins.map(asin => ({
// // //         url: `https://www.amazon.com/dp/${asin}`,
// // //         userData: {
// // //             label: 'ASIN',
// // //         },
// // //     }));

// // //     const requestList = await Apify.openRequestList('asins', detailsProductPages);

// // //     const detailCrawler = new Apify.PuppeteerCrawler({
// // //         requestList,
// // //         handlePageFunction: async ({$, request, page}) => {
// // //             console.log(`Processing ${request.url}`);

// // //             const results = {
// // //                 title: documentQuerySelector('#titleSection #productTitle').text().trim(),
// // //                 url: request.url,
// // //                 description: page.$('.a-unordered-list.a-vertical.a-spacing-mini li .a-list-item').text().trim().replace(/\n/g,''),
// // //                 keyword: input.keyword,
// // //                 sellerName: page.$('#tabular-buybox .tabular-buybox-text').text().trim(),
// // //                 price: page.$('.a-box-inner #price_inside_buybox').text().trim() || $('.a-lineitem #priceblock_ourprice'),
// // //                 shippingPrice: page.$('.a-box-inner .a-size-base.a-color-secondary').text().trim().replace(/\n/g,'') || $('.a-size-base.a-color-secondary').text().trim().replace(/\n/g,'')
// // //             } 
// // //             await Apify.pushData(results)
// // //         }
// // //     }) 
// // //     await detailCrawler.run();
// // // });


// const Apify = require('apify')

// Apify.main( async () => {
//     const results = []
//     const asins = []
//     const input = await Apify.getValue('INPUT')

//     const requestQueue = Apify.openRequestQueue()

//     const requestList = await Apify.openRequestList('start-url', [{
//         url: `https://www.amazon.com/s/?field-keywords=${input.keyword}`,
//         userData: { label: "START" },
//     }]);


//     const handlePageFunction = async ({$, request}) => {
//         switch(request.userData.label) {
//             case 'START':
//                 $('.s-main-slot.s-result-list.s-search-results.sg-row div').each((i, el) => {
//                     const text = $(el).attr('data-asin')
//                     if (text != undefined && text != '') {
//                         asins.push(text)
//                     }
//                 });
//                 for (const ASIN of asins) {
//                     await requestQueue.addRequest({
//                         url: `https://www.amazon.com/dp/${ASIN}`,
//                         userData: { label: 'PRODUCT', ASIN, keyword },
//                     });
//                 }
//             case 'PRODUCT':
//                 console.log(`Processing product ${request.url}`);

//                 const productDetails = {
//                     title: $('#titleSection #productTitle').text().trim(),
//                     url: request.url,
//                     description: $('.a-unordered-list.a-vertical.a-spacing-mini li .a-list-item').text().trim().replace(/\n/g,''),
//                 } 
//                 results.push(productDetails)
                
//                 for (const ASIN of asins) {
//                     await requestQueue.addRequest({
//                         url: `https://www.amazon.com/gp/offer-listing/${ASIN}`,
//                         userData: { label: 'OFFERS', ASIN, keyword },
//                     });
//                 }
//             case 'OFFERS':
//                 console.log(`Processing offers of product ${request.url}`);

//                 const pinnedOffer = {
//                     pinnedPrice: $('#pinned-offer-top-id .a-offscreen'),
//                     pinnedSellerName: $('.a-fixed-left-grid a'),
//                     pinnedShippingPrice: $('.a-color-secondary.a-size-base')
//                 }

//                 const additionalOffers = await $('#aod-offer', (offers) => {
//                     return offers.map((offer) => {
//                         const sellerName = offer.querySelector('#aod-offer-soldBy [aria-label]').textContent?.trim()
//                         const price = offer.querySelector('.a-offscreen').textContent?.trim();
//                         const shippingPrice = offer.querySelector('#delivery-message').textContent?.trim();
//                         return {
//                             sellerName,
//                             price,
//                             shippingPrice,
//                         };
//                     })
//                 })
                
                
//                 // let result = {
//                 //     price:
//                 //     sellerName:
//                 //     shippingPrice
//                 // }
//         }   
//     }
// })




// const Apify = require('apify');
// const { handleOffers } = require('./src/handleOffers');
// const { handleProduct } = require('./src/handleProduct');
// const { handleSearch } = require('./src/handleSearch');

// const { utils: { log } } = Apify;

// Apify.main(async () => {
//     const { keyword } = await Apify.getInput();
//     log.info("Starting crawler with keyword: " + keyword);
//     const startUrl = `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`
//     const requestList = await Apify.openRequestList('start-url', [{
//         url: startUrl,
//         userData: { label: "SEARCH" },
//     }]);


//     const requestQueue = await Apify.openRequestQueue();

//     const crawler = new Apify.PuppeteerCrawler({
//         requestList,
//         requestQueue,
//         useSessionPool: true,
//         persistCookiesPerSession: true,
//         handlePageFunction: async (context) => {
//             const { page, request } = context;
//             const { url, userData: { label } } = request;

//             log.info(`Page opened. ${label} ${url} `);

//             switch(label) {
//                 case('SEARCH'):
//                     return handleSearch(context)
//                 case('PRODUCT'):
//                     return handleProduct(context)
//                 case('OFFERS'):
//                     return handleOffers(context)
//             }
//         },
//     });

//     log.info('Starting the crawl.');
//     await crawler.run();

//     // await Apify.call('apify/send-mail', {
//     //     to: 'lukas@apify.com',
//     //     subject: 'Pavlo Nikolaiev. This is for the Apify Tutorials',
//     //     text: `The link to the dataset:
//     // https://api.apify.com/v2/datasets/9VxKayRSyUdbY1OID/items`,
//     // });

// });


// const Apify = require('apify');

// const {
//     utils: {
//         log
//     }
// } = Apify;

// Apify.main(async () => {
//     const delay = ms => new Promise(res => setTimeout(res, ms));
//     const {
//         keyword
//     } = await Apify.getInput();
//     log.info("Starting crawler with keyword: " + keyword);
//     const startUrl = `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`
//     const requestList = await Apify.openRequestList('start-url', [{
//         url: startUrl,
//         userData: {
//             label: "START"
//         },
//     }]);


//     const requestQueue = await Apify.openRequestQueue();

//     const crawler = new Apify.PuppeteerCrawler({
//         requestList,
//         requestQueue,
//         useSessionPool: true,
//         persistCookiesPerSession: true,
//         handlePageFunction: async (context) => {
//             const {
//                 url,
//                 userData: {
//                     label
//                 },
//                 session
//             } = context.request;
//             const request = context.request;
//             log.info('Page opened.', {
//                 label,
//                 url
//             });
//             const page = context.page;
//             const baseURl = 'https://www.amazon.com';

//             // START: fetch all ASIN of the products found
//             if (request.userData.label === "START") {

//                 // Get all the products ASIN
//                 // const ASINList = $('div[data-asin]').map((i, elem) => $(elem).attr('data-asin')).get();
//                 let ASINList = await page.$$eval('div[data-asin]', (els) => els.map((el) => el.getAttribute('data-asin')));
//                 log.info('Found ASIN:' + ASINList);
//                 for (const ASIN of ASINList) {

//                     // Filter blank ASINs
//                     if (ASIN) {
//                         let url = new URL(`https://www.amazon.com/dp/${ASIN}`, baseURl)
//                         log.info('Enqueuing product URL:', {
//                             href: url.href
//                         });
//                         await requestQueue.addRequest({
//                             url: url.href,
//                             userData: {
//                                 label: "DETAIL",
//                                 ASIN: ASIN,
//                             },
//                         });
//                     }
//                 }
//             }

//             // DETAIL: finding general product info + offers URL
//             else if (request.userData.label === "DETAIL") {

//                 // Find title, url and description of the product
//                 const title = await page.$eval('#titleSection #title', ((el) => el.textContent ?.trim()))
//                 // Sometimes, the page doesn't contain any description
//                 const description = await page.$eval('#feature-bullets li', ((el) => el.textContent ?.trim()))
//                 log.info(`Title: ${title}, Description: ${description}`)

//                 const ASIN = request.userData.ASIN;
            
            

//                 // Query offers
//                 let offersUrl = new URL(`https://www.amazon.com/gp/offer-listing/${ASIN}`);
//                 await requestQueue.addRequest({
//                     url: offersUrl.href,
//                     userData: {
//                         label: 'OFFERS',
//                         data: {
//                             url: request.url,
//                             title: title,
//                             description: description,
//                             keyword: keyword 
//                         },
//                     }
//                 });

//             }

//             // OFFERS: Get the product's offers data
//             else if (request.userData.label === "OFFERS") {
//                 const productInfo = {
//                     title: request.userData.data.title,
//                     url: request.url,
//                     description: request.userData.data.description,
//                     keyword: keyword
//                 };
//                 // const sellerNameP = await page.$eval('#aod-offer-soldBy [aria-label]',
//                 //     ((el) => el.textContent ?.trim()))
//                 // const priceP = await page.$eval('#pinned-offer-top-id .a-offscreen',
//                 //     ((el) => el.textContent ?.trim()))
//                 // const shippingPriceP = await page.$eval('#aod-bottlingDepositFee-0 + .a-size-base',
//                 //     ((el) => { 
//                 //         if (!el) { 
//                 //             el == 'Free' } else el.textContent ?.trim()})) 

//                 const sellerNameP = 'testSeller'
//                 const priceP = 'testPrice'
//                 const shippingPriceP= 'testShip'

//                 const [
//                     sellerName,
//                     price,
//                     shippingPrice,
//                 ] = await Promise.all([
//                     sellerNameP + ' This is pinned offer',
//                     priceP + ' This is pinned offer',
//                     shippingPriceP + ' This is pinned offer',
//                 ]);

//                 await Apify.pushData({
//                     ...productInfo,
//                     sellerName,
//                     price,
//                     shippingPrice,
//                 });


//                 const offersInfos = await page.$$eval('#aod-offer',
//                     (offers) => {
//                         return offers.map((offer) => {
//                             const sellerName = offer.querySelector('#aod-offer-soldBy [aria-label]')
//                                 .textContent ?.trim();
//                             const price = offer.querySelector('.a-offscreen').textContent;

//                             const shippingPriceEl = offer.querySelector('#delivery-message');
//                             const shippingPrice = shippingPriceEl ?
//                                 shippingPriceEl.textContent ?.trim()
//                                 .split(' ')[0]
//                                 .toLowerCase() :
//                                 'free';

//                             return {
//                                 sellerName,
//                                 price,
//                                 shippingPrice,
//                             };
//                         });
//                     });

//                 for (const offerInfo of offersInfos) {
//                     await Apify.pushData({
//                         ...productInfo,
//                         ...offerInfo,
//                     });
//                 }

//             }
//         },
//     });

//     log.info('Starting the crawl.');
//     await crawler.run();
//     const dataset = await Apify.openDataset();

//     // await Apify.call('apify/send-mail', {
//     //     to: 'lukas@apify.com',
//     //     subject: 'Pavlo Nikolaiev. This is for the Apify Tutorials',
//     //     text: `The link to the dataset:
//     // https://api.apify.com/v2/datasets/9VxKayRSyUdbY1OID/items`,
//     // });

// });

let x = '104 other options1'

let y = x.split(' ')[0] - 0

console.log(typeof(y) + y)
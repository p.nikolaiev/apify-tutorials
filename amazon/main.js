const Apify = require('apify');

const { handleOffersList } = require('./src/handleOffersList');
const { handlePageOffers } = require('./src/handlePageOffers');
const { handleProduct } = require('./src/handleProduct');
const { handleSearch } = require('./src/handleSearch');
const { storageKeys, defaultUrls, LABELS } = require('./src/const');

const {
    utils: {
        log
    }
} = Apify;

Apify.main(async () => {
    const { keyword } = await Apify.getInput();

    log.info("Starting crawler with keyword: " + keyword);

    // Getting of start url
    const requestList = await Apify.openRequestList('start-url', [{
        url: `${defaultUrls.searchUrl}${keyword}`,
        userData: {
            label: LABELS.SEARCH
        },
    }]);

    const requestQueue = await Apify.openRequestQueue();
    const proxyConfiguration = await Apify.createProxyConfiguration({
        groups: ['BUYPROXIES94952'],
    });

    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        proxyConfiguration,
        useSessionPool: true,
        persistCookiesPerSession: true,
        sessionPoolOptions: {
            maxPoolSize: 100,
            sessionOptions: {
                maxUsageCount: 5,
            },
        },
        launchContext: {
            stealth: true
        },
        handlePageFunction: async (context) => {
            const { page, crawler: { requestQueue }, request, session } = context;
            const { url, userData: { label, keyword } } = request;

            log.info('Opened page: ', { label, url });

                try {
                    const pageTitle = await page.title();
                    if (
                        pageTitle === "Amazon.com" ||
                        pageTitle === "Sorry! Something went wrong!"
                    ) {
                        log.error("Page is blocked. Session will be retired!");
                        session.retire();
                    }
                } catch (err) {
                    console.log("SESSION ERROR MESSAGE: ", err.message, "\n");
                    throw err;
                }

            switch(label) {
            // SEARCH: fetch all ASINs from search page
                case LABELS.SEARCH:
                    return handleSearch(context)
            // PRODUCT: fetch title, url and description of the products
                case LABELS.PRODUCT:
                    return handleProduct(context)
                case LABELS.OFFERS_PAGE:
                    return handlePageOffers(context)
            // OFFERS: fetch all offers each of the products
                case LABELS.OFFERS:
                    return handleOffersList(context)
            }
        },
        handleFailedRequestFunction: async ({ request }) => {
                log.error(`Request ${request.url} failed too many times`);
                const debugDataSet = await Apify.openDataset('debug-amazon');
                await debugDataSet.pushData({
                    '#debug': Apify.utils.createRequestDebugInfo(request),
                });
            },
    });

    Apify.events.on('migrating', () => {
        Apify.setValue(storageKeys.STATE, stats)
    });

    const showStats = setInterval(async () => {
        const stats = await Apify.getValue(storageKeys.STATE)
        console.log(stats);
        if (await requestQueue.isFinished()) {
            clearInterval(showStats);
        }
    }, storageKeys.STATS_INTERVAL);

    log.info('Starting the crawl.');

    await crawler.run();
    
    // Getting of dataset url
    const dataUrl = `https://api.apify.com/v2/datasets/${process.env.APIFY_DEFAULT_DATASET_ID}/items`;
    console.log(dataUrl);
    // Sending email
    // await Apify.call('apify/send-mail', {
    //     to: 'lukas@apify.com',
    //     subject: 'Pavlo Nikolaiev. This is for the Apify Tutorials',
    //     text: `Data results for such keyword: ${keyword}. The link to the dataset: ${dataUrl}`,
    // });
})
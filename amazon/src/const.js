exports.storageKeys = {
    STATE: 'offers-by-asin',
    STATS_INTERVAL: 20000,
}

exports.defaultUrls = {
    searchUrl: 'https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=',
}

exports.LABELS = {
    SEARCH: 'SEARCH',
    PRODUCT: 'PRODUCT',
    OFFERS: 'OFFERS',
    OFFERS_PAGE: 'OFFERS_PAGE',
}
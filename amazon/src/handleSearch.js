const Apify = require('apify');
const {
    utils: {
        log
    }
} = Apify;

const { LABELS: { PRODUCT } } = require('./const')

exports.handleSearch = async ({ page, crawler: { requestQueue } }) => {
    log.info('Search of ASINS started')
    // Get all the products ASIN
    let ASINList = await page.$$eval('div[data-asin]', (els) => els.map((el) => el.getAttribute('data-asin')))
    log.info('Found such ASINs:' + ASINList)
    log.info(`Found ${ASINList.length} ASINs`)
    for (const ASIN of ASINList) {

        // Filter for correct ASINS
        if (ASIN) {
            let url = `https://www.amazon.com/dp/${ASIN}`
            await requestQueue.addRequest({
                url: url,
                userData: {
                    label: PRODUCT,
                    ASIN: ASIN,
                },
            });
        }
    }
}
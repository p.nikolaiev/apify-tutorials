const Apify = require('apify');
const {
    utils: {
        log
    }
} = Apify;

const { LABELS: { OFFERS_PAGE } } = require('./const');

exports.handleProduct = async ({ page, request, keyword, crawler: { requestQueue } }) => {
    // Get title and description of the product
    const title = await page.$eval('#titleSection #productTitle', ((el) => el.textContent ?.trim()));
    const description = await page.$eval('#feature-bullets li', ((el) => el.textContent ?.trim()));
    log.info(`Title: ${title}, Description: ${description}`);

    const ASIN = request.userData.ASIN;

    // Get all offers of the product
    let offersUrl = `https://www.amazon.com/gp/offer-listing/${ASIN}`
    await requestQueue.addRequest({
        url: offersUrl,
        userData: {
            label: OFFERS_PAGE,
            ASIN: ASIN,
            data: {
                url: request.url,
                title: title,
                description: description,
                keyword,
            },
        }
    });

}
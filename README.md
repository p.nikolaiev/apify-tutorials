Pavlo Nikolaiev

Tutorial II - Apify SDK

Where and how can you use JQuery with the SDK?
- We can use JQuery with Apify SDK with the Puppeteer crawler.

What is the main difference between Cheerio and JQuery?
- JQuery used for manipulating the DOM of a HTML page. Cheerio parses html from HTTP responses. Cheerio it is a implementation of jQuery core but doesn’t have full JQuery API

When would you use CheerioCrawler and what are its limitations?
- Using Cheerio for scraping it is good idea for static pages, but, when website requires JavaScript to display the content we need to use PuppeteerCrawler or PlaywrightCrawler instead, because these loads the pages using full-featured headless Chrome browser.

What are the main classes for managing requests and when and why would you use one instead of another?
- We have two main classes for managing requests: RequestQueue, RequestList.
RequestQueue presents a dynamic queue of Requests. One that can be updated at runtime by adding more pages - Requests to process.
RequestList it is a static, immutable list of URLs and we cannot add any requests there during the process.

How can you extract data from a page in Puppeteer without using JQuery?
- We can use standard methods of browser like document.querySelector()

What is the default concurrency/parallelism the SDK uses?
- By default, the max amount of memory to be used is set to one quarter of total system memory. If we hadn't set minConcurrency parameter before, it will scale up automatically. maxConcurrency and desiredConcurrency default values equal minConcurrency.r.


Tutorial III - Apify Actors & Webhooks

How do you allocate more CPU for your actor run?
- This happens automatically when we allocate or increase memory to run Actor.

How can you get the exact time when the actor was started from within the running actor process?
- Information about actor is stored in the appropriate environment variable.

Which are the default storages an actor run is allocated (connected to)?
- Each actor have 3 default storages: Datasets, Key-value store, Request queue

Can you change the memory allocated to a running actor?
- To change the memory for actor we need to restart actor.

How can you run an actor with Puppeteer in a headful (non-headless) mode?
- To launch Puppeteer in a headful (non-headless) mode we can add the option headless: false. 
Example: const browser = await puppeteer.launch({headless: false});

Imagine the server/instance the container is running on has a 32 GB, 8-core CPU. What would be the most performant (speed/cost) memory allocation for CheerioCrawler? (Hint: NodeJS processes cannot use user-created threads)
- Cheerio crawlers cannot use more than 1 CPU core, so using more than 4GB memory and more than 1 CPU core not make sense. 


(Bonus - Docker)
What is the difference between RUN and CMD Dockerfile commands?
- RUN lets execute commands inside of your Docker image
CMD lets define a default command to run when your container starts.

Does your Dockerfile need to contain a CMD command (assuming we don't want to use ENTRYPOINT which is similar)? If yes or no, why?
- We must specify at least one instruction (ENTRYPOINT or CMD) to run docker file. Otherwise we get an error

How does the FROM command work and which base images Apify provides?
- The FROM instruction initializes a new build stage and sets the Base Image for subsequent instructions. 
  Base Docker images:
  - Node.js 14 on Alpine Linux (apify/actor-node)
  - Node.js 14 + Puppeteer + Chrome on Debian (apify/actor-node-puppeteer-chrome)
  - Node.js 14 + Playwright + Chrome on Debian (apify/actor-node-playwright-chrome)


  Tutorial IV - Apify CLI & Source Code

  Do you have to rebuild an actor each time the source code is changed?
  - Yes, we should to rebuild an actor each time if we push new source code.

  What is the difference between pushing your code changes and creating a pull request?
  - When we push some changes in code, it's also changes the state in our source branch. But when we create a pull request we merge changes between different branches, we can see differences and other info about them. 

  How does the apify push command work? Is it worth using, in your opinion?
  - It pushes the source code and builds an image but it's not worth if the target actor already exists in Apify account because it will be overwritten.


  Tutorial V - Tasks, Storage, Apify API & Client

  What is the relationship between actor and task?
  - With Actor Tasks we can create different configuration for same actor to perform a specific job.

  What are the differences between default (unnamed) and named storage? Which one would you choose for everyday usage?
  - Unnamed storages have limited time of saving dataset (14 days). Named storages can saves the data indefinitely. If I will use the data at once after scraping, I will be use unnamed storages, in other cases better to used named storages.

  What is the relationship between the Apify API and the Apify client? Are there any significant differences?
  - Apify client support the handling of the the API calls.

  Is it possible to use a request queue for deduplication of product IDs? If yes, how would you do that?
  - The request queue support the function adding of requests without any duplicate.

  What is data retention and how does it work for all types of storage (default and named)?
  - Data retention it's a function of Apify to save in storage any scraped data. Default storages have limited time of saving dataset (14 days). Named storages saves data indefinitely.

  How do you pass input when running an actor or task via the API?
  - Using a POST method when running the actor using the Apify API


  Tutorial VI - Apify Proxy & Bypassing Antiscraping Software

  What types of proxies does the Apify Proxy include? What are the main differences between them?
  - The basic is one is the Datacenter proxy.
    Google SERP proxy - for the Google search scrapers.
    Residential proxy - the servers are positioned geographicaly in some living areas to be more user-like.

  Which proxies (proxy groups) can users access with the Apify Proxy trial? How long does this trial last?
  - 30-days basic trial gives the access to Datacenter proxies.

  How can you prevent a problem that one of the hardcoded proxy groups that a user is using stops working (a problem with a provider)? What should be the best practices?
  - I can use SessionPool which can filter out blocked or non-working proxies and actor does not retry requests for such proxies.

  Does it make sense to rotate proxies when you are logged in?
  - It will not make sense, because "user" can't login from one IP and does many different actions on site from another IP.

  Construct a proxy URL that will select proxies only from the US (without specific groups).
  - http://<session>,country-US:<password>@proxy.apify.com:8000.

  What do you need to do to rotate proxies (one proxy usually has one IP)? How does this differ for Cheerio Scraper and Puppeteer Scraper?
  - We can use a SessionPool for crawler to rotate the IPs. Puppeteer rotates IP after the browser changes and by default it will use the same IP, unlike Cheerio.

  Try to set up the Apify Proxy (using any group or auto) in your browser. This is useful for testing how websites behave with proxies from specific countries (although most are from the US). You can try Switchy Omega extension but there are many more. Were you successful?
  - I used the Express VPN and it was good practice.

  Name a few different ways a website can prevent you from scraping it.
  - Blocking IP, if was a lot of requests from same IP.
  - Using coockies.
  - Using of the captcha.
  
  Do you know any software companies that develop anti-scraping solutions? Have you ever encountered them on a website?
  - I know Radware Bot Manager. It used for anti-scraping, but I never encountered with them


  Tutorial VII - Actor Migrations & Maintaining State

  Actors have a Restart on error option in their Settings. Would you use this for your regular actors? Why? When would you use it, and when not?
  - I will not use this option on my regular actors, because many Errors happens on websites which block requests and we will have many restarts. Maybe I will use this option for actors which will not expected any errors from websites.

  Migrations happen randomly, but by setting Restart on error and then throwing an error in the main process, you can force a similar situation. Observe what happens. What changes and what stays the same in a restarted actor run?
  - All processes are stopped. If you not saved your state, the actor run will restart and progress will be lost.

  Why don't you usually need to add any special code to handle migrations in normal crawling/scraping? Is there a component that essentially solves this problem for you?
  - We don't need to add special code because special events, like migration and persistState, are emitted and handled by Apify SDK

  How can you intercept the migration event? How much time do you need after this takes place and before the actor migrates?
  - We can listening the migration and persistState and if a migration event occurs, we only have a few seconds to save our work.

  When would you persist data to a default key-value store and when would you use a named key-value store?
  - If we dont need to save key-value store for a long time, we can use defaut stores, but if would use key-value store after long time, we need to use a named stores


    
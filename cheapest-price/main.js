const Apify = require('apify');

Apify.main(async () => {
    // get the dataset with all offers
    const { resource: { defaultDatasetId } } = await Apify.getInput();
    const { data: offers } = Apify.openDataset(defaultDatasetId);

    const cheapestOffers = {};

    // get the cheapest offers
    offers.forEach((offer) => {
        if (!cheapestOffers[offer.url] || cheapestOffers[offer.url].price > offer.price) {
            cheapestOffers[offer.url] = offer;
        }
    });

    const finalData = [];
    for (const key of Object.keys(cheapestOffers)) {
        finalData.push(cheapestOffers[key]);
    }
    await Apify.pushData(finalData);
});

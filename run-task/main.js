/* eslint-disable brace-style */
const Apify = require('apify');
const ApifyClient = require('apify-client');
const axios = require('axios');

const TOKEN = process.env.APIFY_TOKEN;

Apify.main(async () => {
    let data;
    const { memory, fields, useClient, maxItems, taskId, userId } = await Apify.getInput();

    // Get the data using Apify client
    if (useClient) {
        const apifyClient = new ApifyClient({
            userId,
            token: TOKEN,
        });
        const actRun = await apifyClient.tasks.runTask({
            taskId,
            memory,
            waitForFinish: 300,
        });
        const datasets = await apifyClient.datasets;
        data = await datasets.getItems({
            datasetId: actRun.defaultDatasetId,
            maxItems,
            fields,
            format: 'csv',
        });
    }
    // Get the data using API
    else {
        const { data: response } = await axios.post(
            `https://api.apify.com/v2/actor-tasks/${taskId}/runs?token=${TOKEN}&memory=${memory}&waitForFinish=300`,
        );
        const { data: { defaultDatasetId } } = response;
        const allFields = fields.join();
        const { data: responseData } = await axios.get(
            `https://api.apify.com/v2/datasets/${defaultDatasetId}/items?token=${TOKEN}&format=csv&limit=${maxItems}&fields=${allFields}`,
        );
        data = responseData;
    }
    await Apify.setValue('OUTPUT', data, {
        contentType: 'text/csv',
    });
});
